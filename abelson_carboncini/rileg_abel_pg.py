#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------python----------------rileg_abel_pg.py--#
#                                                                       #
#                Rileggendo “A Beginner’s Guide to Logo”                #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2021--#

"""
Procedure relative all'articolo di Claudio Carboncini:
Rileggendo “A Beginner’s Guide to Logo”

https://sjnecura.netlify.app/posts/rileggendo-abelson/

Le linee precedute da "#" non vengono eseguite, sono dei commenti.
Per commentare una linea: <Ctrl-d>, per decommentarla: <Ctrl-shift-d>.
"""

import turtle as pt # Carica la libreria della tartaruga
import random       # Carica la libreria per i numeri casuali (per: game)

piano = pt.Screen() # Usato per individuare le dimensioni dello schermo
tina = pt.Turtle()  # classe che esegue i comandi della tartaruga
tina.speed(0)       # per rendere più rapido il disegno

def square():
    """Quadrato con lato fisso."""
    for cont in range(4):
        tina.forward(50)
        tina.right(90)

##square()

def square(size):
    """Quadrato di lato size."""
    for cont in range(4):
        tina.forward(size)
        tina.right(90)

##square(100)

def design():
    """Disegno fatto con quadrati."""
    for cont in range(6):
        tina.forward(20)
        tina.right(60)
        square(75)

##design()
##
##tina.reset()
##tina.speed(0)

def sq(size):
    """Quadrato prodotto da una funzione ricorsiva."""
    tina.forward(size)
    tina.right(90)
    sq(size)
    
'''
Da notare che Python non supporta la tail recursion elimination,
perciò se lasciamo andare la funzione ad un certo punto termina
con il messaggio:
"RecursionError: maximum recursion depth exceeded in ..."

Per terminare la funzione prima del messaggio di errore, bisogna chiudere
la finestra grafica.
'''

##sq(200)

def poly(size, angle):
    """Poligono prodotto da una funzione ricorsiva."""
    tina.forward(size)
    tina.right(angle)
    poly(size, angle)

##poly(80, 160)
      
'''
Siccome per me è fastidioso chiudere la finestra per terminare
l'esecuzione, ho aggiunto la condizione di terminazione
alle funzioni ricorsive.
'''

def polyspi(size, angle):
    """Poligono prodotto da una funzione ricorsiva."""
    if size > 200: return
    tina.forward(size)
    tina.right(angle)
    polyspi(size+3, angle)

##polyspi(1, 160)

'''
Le due seguenti funzioni non centrano con il lavoro di Abelson,
né hanno importanza da un punto di vista logico.
Le ho trovate utili per disegnare quattro figure distanziate
in una finestra grafica.
'''

def moveto(xpos, ypos):
    """Muove tina nel punto indicato e le assegna la direzione 0."""
    tina.pu()
    tina.setpos(xpos, ypos)
    tina.setheading(0)
    tina.pd()
    

def quattrofigure(funzione, arg1, arg2, arg3, arg4):
    """Disegna le quattro figure equidistanziate."""
    xshift, yshift = piano.window_width() // 4, piano.window_height() // 4
    moveto(-xshift, +yshift)
    funzione(*arg1)
    moveto(+xshift, +yshift)
    funzione(*arg2)
    moveto(-xshift, -yshift)
    funzione(*arg3)
    moveto(+xshift, -yshift)
    funzione(*arg4)

##quattrofigure(polyspi, (1, 160), (1, 120), (1, 90), (1, 89))

def spinsquare(size, angle):
    """Poligono prodotto da una funzione ricorsiva."""
    if size > 100: return
    square(size)
    tina.right(angle)
    spinsquare(size+3, angle)

##spinsquare(5, 10)

def average(x, y):
    """Restituisce la media dei due numeri x e y."""
    return (x + y) / 2

##print(average(2, 3))
##print(average(1, 2) + average(3, 4) )
##print(average(average(1, 2), 3))
##print(average(37, 45))
##print(average(.37, .45))

'''
Da notare il risultato dell'ultima esecuzione di average
che costringe a riflettere sul significato dei numeri periodici e
su come sono memorizzati i numeri nei computer.
'''

'''
Prima versione di "game" seguendo lo stile più vicino a LOGO
proposto da Abelson.

Nota:
"tina" e "piano" sono oggetti globali definiti all'inizio del modulo.
'''

def game():
    """In questo gioco il computer sceglie una posizione segreta,
il gocatore deve avvicinarsi ad una distanza minore di 20
indicando un angolo e una lunghezza."""
    global xpt, ypt
    piano.reset()
    semiwidth = piano.window_width() // 2 - 20
    semiheight = piano.window_height() // 2 - 20
    xpt = random.randrange(-semiwidth, +semiwidth)
    ypt = random.randrange(-semiheight, +semiheight)
    play()

def play(moves=0):
    """Esegue il gioco."""
    if checkwin():
        print(f"you ar win in {moves} moves.")
        piano.cv()
        return
    makemove()
    play(moves=moves+1)

def makemove():
    """Esegue una mossa del giocatore."""
    print("\nTurn left how much?")
    tina.left(readnumber())
    print("Go forward how much?")
    tina.forward(readnumber())

def readnumber():
    """Legge un numero assicurandosi che sia proprio un numero
e lo restituisce come risultato."""
    try:
        return int(input("Scrivi un numero: "))
    except:
        print("Mi serve proprio un numero!")
    return readnumber()

def checkwin():
    """Restituisce True se la distanza dall'attuale posizione di tina
dall'obiettivo è minore di 20."""
    d = tina.distance(xpt, ypt)
    print(f"\nDistance to targhet is: {d}")
    return d < 20

# game()


'''
Seconda versione di "game" seguendo lo stile più Pythonico:
- evito le variabili globali e 
- sostituisco la ricorsione con cicli while.

Nota:
"tina" e "piano" sono oggetti globali definiti all'inizio del modulo.

L'esecuzione del gioco si ottiene con l'esecuzione del comando:
>>> game()

la funzione principale è definita subito sopra la chiamata a game e
le altre funzioni sono scritte prima del loro uso quindi prima di game
è definito setup e prima ancora play e così via...

Poiché il finale del gioco mi lasciava insoddisfatto, ho aggiunto
la funzione drawtarghet.
'''

def setup():
    """Predispone l'obiettivo."""
    piano.reset()
    semiwidth = piano.window_width() // 2 - 20
    semiheight = piano.window_height() // 2 - 20
    return (random.randrange(-semiwidth, +semiwidth),
            random.randrange(-semiheight, +semiheight))

def checkwin(target):
    """Restituisce True se la distanza dall'attuale posizione di tina
dall'obiettivo è minore di 20."""
    d = tina.distance(target)
    print(f"\nDistance to targhet is: {d}")
    return d < 20

def readnumber():
    """Legge un numero assicurandosi che sia proprio un numero
e lo restituisce come risultato."""
    try:
        return int(input("Scrivi un numero: "))
    except:
        print("Mi serve proprio un numero!")
    return readnumber()

def makemove():
    """Esegue una mossa del giocatore."""
    print("Turn left how much?")
    tina.left(readnumber())
    print("Go forward how much?")
    tina.forward(readnumber())

def drawtarghet(targhet, radius):
    """Disegna due cerchi attorno all'obiettivo."""
    tina.stamp()
    tina.hideturtle()
    tina.up()
    tina.setpos(targhet)
    tina.setheading(0)
    tina.forward(3)
    tina.left(90)
    tina.down()
    tina.circle(3)
    tina.up()
    tina.right(90)
    tina.forward(radius-3)
    tina.left(90)
    tina.down()
    tina.circle(radius)
    
def play(targhet, radius):
    """Esegue il gioco."""
    moves = 0
    while not checkwin(targhet):
        moves += 1
        makemove()
    else:
        print(f"you ar win in {moves} moves.")
        drawtarghet(targhet, radius)

def game(radius=20):
    """In questo gioco il computer sceglie una posizione segreta,
il gocatore deve avvicinarsi ad una distanza minore di 20
indicando un angolo e una lunghezza."""
    play(setup(), radius)

##game()

# Liste

'''
In Pyhton c'è la struttura dati "list".
Assomiglia molto alla struttura lista di LOGO, ma non ha i comandi
"first", "last", "butfirst", "butlast", "fput", "lput".

Per ottenere il comportamento dei comandi LOGO, in Python si usano
gli indici e l'affettamento:
   LOGO              Pyhton
first lista         lista[0]
last lista          lista[-1]
butfirst lista      lista[1:]
butlast lista       lista[:-1]
fput ele lista      lista.insert(0, ele)
lput ele lista      lista.append(ele)
'''

lista0 = [1, 2, 'buckle', 'my', 'shoe']
lista1 = [['Peter', 'Pan'], 'Wendy', 'John']
print(lista0)
print(lista1)
print()
print("FIRST")
print(lista0[0])
print(lista1[0])
print()
print("LAST")
print(lista0[-1])
print(lista1[-1])
print()
print("BUTFIRST")
print(lista0[1:])
print(lista1[1:])
print()
print("BUTLAST")
print(lista0[:-1])
print(lista1[:-1])
print()
print("FPUT")
lista0.insert(0, 5)
print(lista0)
lista0.insert(0, lista1[0])
print(lista0)
print()
print("SENTENCE")
lista2 = lista1[0]
print(lista2)
lista2.extend(['buckle', 'my', 'shoe'])
print(lista2)

def double(lista):
    """Restituisce una copia di lista ripetuta."""
    return lista * 2

print(double(['Oom', 'pah']))
print(double(double(['Oom', 'pah'])))

# Giocare con i testi

'''
In logo ci sono le parole e le liste di parole
che possono essere viste come frasi.
Quindi un testo è visto come una lista di parole.

In Python un testo è visto come una stringa.
La stringa è una sequenza di caratteri
racchiusi tra apici singoli o doppi.

Una stringa può essere trasformata in una lista di parole con "split" e
una lista di parole può essere trasformata in una stringa con "join".
'''

##Le seguenti variabili sono liste di stringhe.
##e formano la nostra base di dati
names = ['John', 'Dorothy', 'Aunt Em', 'Occupant']
phrases = ["Wish you were here.", "Weather's great!",
           "Surf's up.", "Everyone's fine."]
closings = ["Love", "See you soon", "Write soon"]

def getrandom(lista):
    """Restituisce un nome a caso."""
    return random.choice(lista)

def getbody():
    """Una volta su due circa, stampa un testo composto da più frasi."""
    result = getrandom(phrases)
    if unavoltasuennecirca(2):
        result += ' ' + getbody()
    return result

def postcard():
    """Restituisce il testo di una cartolina."""
    return f"""
Dear {getrandom(names)}
{getbody()}
{getrandom(closings)} -- {getrandom(names)}
"""

def unavoltasuennecirca(enne):
    """Restituisce  vero circa una volta su enne."""
    return random.randrange(enne) == 0

def learn_new_phrase():
    """Aggiunge una frase, se non vuota, alla lista delle frasi."""
    phrase = input("Please type in a new phrase: ")
    if phrase != "":
        phrases.append(phrase)

def printpostcard(num):
    """Stampa num cartoline."""
    for cont in range(num):
        if unavoltasuennecirca(5):
            learn_new_phrase()
        print(postcard())

##printpostcard(12)

# Pavimentazioni ricorsive

'''
Il modulo base è un disegno realizzato nei pressi
di un vertice di un quadrato: proc.
Questo disegno viene ripetuto per ogni vertice del quadrato.
Il modulo base può essere ruotato, di angoli multipli di 90°.
'''

'''
Prima versione: come proposto nell'articolo di Abelson.

Nota:
"tina" è un oggetto globale definito all'inizio del modulo.
'''

def corner(a, proc, s):
    """Disegna proc ruotato di a*90° con la scala s."""
    tina.forward(s/2)
    tina.right(a*90)
    drawfigure(proc, s/2)
    tina.left(a*90)
    tina.back(s/2)
    tina.right(90)

def drawfigure(proc, arg):
    proc(arg)

def glue1(proc, s):
    """Incolla 4 procedure una senza modificare a."""
    for cont in range(4):
        corner(0, proc, s)

def glue2(proc, s):
    """Incolla 4 procedure una usando per a la sequenza aa."""
    corner(0, proc, s)
    corner(2, proc, s)
    corner(1, proc, s)
    corner(3, proc, s)

def triangle(dim):
    """Disegno."""
    semiip = dim / 2 * 2**.5
    tina.left(45)
    tina.forward(semiip)
    tina.right(135)
    tina.forward(dim)
    tina.right(90)
    tina.forward(dim)
    tina.right(135)
    tina.forward(semiip)
    tina.right(45)

##triangle(60)
##glue1(triangle, 60)
##glue2(triangle, 60)

'''
Nella funzione corner mancano degli "up" e "down"
per disegnare solo proc e non anche gli spostamenti della tartaruga.

La funzione "drawfigure" è inutile.

Ho scritto una funzione che riunisce i comandi per fare uno spostamento
senza traccia.

La funzione "drawfigure" è inutile.

Non mi piace che corner lasci la tartaruga ruotata rispetto a come
l'ha presa, penso che la rotazione sarebbe megli affidarla a glue.

Ho generalizzato "glue2" fornendo come parametro la sequenza di rotazioni.

Non è immediato come rendere ricorsivo il programma,
e manca, comunque, la condizione di terminazione.
'''

###
# Disegni di base
def triequi(side):
    """Disegna un triangolo equilatero."""
    for cont in range(3):
        tina.forward(side)
        tina.left(120)

def triangle(dim):
    """Disegno."""
    semiip = dim / 2 * 2**.5
    tina.left(45)
    tina.forward(semiip)
    tina.right(135)
    tina.forward(dim)
    tina.right(90)
    tina.forward(dim)
    tina.right(135)
    tina.forward(semiip)
    tina.right(45)

###
# Funzione di supporto per gli spostamenti
def sposta(ang0, lung, ang1):
    """Ruota di ang0, va avanti di lung, ruota di ang1, senza disegnare."""
    tina.up()
    tina.left(ang0)
    tina.forward(lung)
    tina.left(ang1)
    tina.down()
    
###
# Funzione corner ricorsiva
def corner(a, proc, dim):
    """Disegna proc ruotato di a*90° con la scala s."""
    sposta(0, dim/2, -a*90)
    proc(dim/2)
    sposta(+a*90, -dim/2, 0)
    tina.right(90)

##corner(0, triangle, 60)

###
# Funzione glue generalizzata
def glue(sequenzaangoli, proc, dim):
    """Incolla 4 procedure una usando per a la sequenza sequenzaangoli."""
    for ang in sequenzaangoli:
        corner(ang, proc, dim)

##glue((0, 2, 1, 3), triangle, 60)

##triangle(60)
##glue((0, 0, 0, 0), triangle, 60)
##glue((0, 2, 1, 3), triangle, 60)

###
# Funzione di supporto per gli spostamenti
def sposta(ang0, lung, ang1):
    """Ruota di ang0, va avanti di lung, ruota di ang1, senza disegnare."""
    tina.up()
    tina.left(ang0)
    tina.forward(lung)
    tina.left(ang1)
    tina.down()
    
###
# Funzione corner ricorsiva contiene anche glue:
#   se sono esaurite le combinazioni di angoli, disegna proc,
#   altrimenti disegna corner
def corner(sequenzasequenzeangoli, proc, dim):
    """Disegna proc ruotato di a*90° con la scala s."""
    if not sequenzasequenzeangoli:
        proc(dim)
        return
    for ang in sequenzasequenzeangoli[0]:
        sposta(0, dim, -ang*90)
        corner(sequenzasequenzeangoli[1:], proc, dim/2)
        sposta(+ang*90, -dim, 0)
        tina.right(90)

##corner(((0, 0, 0, 0),(0, 2, 1, 3), (3, 2, 1, 0)), triangle, 60)

'''
A questo punto il nome "corner" non è più molto adatto,
lo cambio con "sqtiler", prestando attenzione di cambiare il nome
sia nella definizione della funzione sia nella chioamata ricorsiva.

Tutto il programma, rivisitato, diventa il seguente.
'''

###
# Funzione di supporto per gli spostamenti
def sposta(ang0, lung, ang1):
    """Ruota di ang0, va avanti di lung, ruota di ang1, senza disegnare."""
    tina.up()
    tina.left(ang0)
    tina.forward(lung)
    tina.left(ang1)
    tina.down()
    
###
# Funzione sqtiler ricorsiva contiene anche glue:
#   se sono esaurite le combinazioni di angoli, disegna proc,
#   altrimenti disegna sqtiler
def sqtiler(sequenzasequenzeangoli, proc, dim):
    """Disegna proc ruotato di a*90° con la scala s."""
    if not sequenzasequenzeangoli:
        proc(dim*2)
        return
    for ang in sequenzasequenzeangoli[0]:
        sposta(0, dim, -ang*90)
        sqtiler(sequenzasequenzeangoli[1:], proc, dim/2)
        sposta(+ang*90, -dim, 0)
        tina.right(90)

tina.hideturtle()
##sqtiler(((0, 0, 0, 0),(0, 2, 1, 3), (3, 2, 1, 0), (0, 1, 2, 3)), triangle, 60)
sqtiler(((0, 0, 0, 0),(0, 2, 1, 3), (3, 2, 1, 0), (3, 0, 1, 3)), triequi, 60)















