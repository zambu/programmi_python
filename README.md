# README #

I programmi contenuti in questo repository possono essere eseguiti 
all'interno di in ide come:

* IDLE;
* Spyder;
* ipython

### Cosa contiene ###

Semplici programmi scritti in Python, usando principalmente la libreria:

turtle

### Contributi ###

Ogni contributo è incoraggiato:

* osservazioni
* segnalazione di errori
* proposte
